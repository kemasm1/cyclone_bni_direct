def change_enter_to_underscore(filename: str) -> str:
    return filename.replace("\n", "-")[:-1]
